package test

import (
	"fmt"
	//	"encoding/json"
	"testing"
	//	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/replix/imds-go/pkg/aws"
)

func TestAwsInstanceMetadata(t *testing.T) {
	if aws.IsAws() {
		imds := aws.New()
		metadata := imds.RecursiveLoad()
		fmt.Printf("%#v", metadata)

		assert.Contains(t, metadata, "ami-id")
		assert.IsType(t, metadata["ami-id"], "")
		assert.Contains(t, metadata, "block-device-mapping")
		assert.IsType(t, metadata["block-device-mapping"], map[string]interface{}{})
	}
}

func TestAwsTypedInstanceMetadata(t *testing.T) {
	if aws.IsAws() {
		imds := aws.New()
		instance, err := imds.LoadInstanceMetadata()
		assert.NoError(t, err)
		fmt.Printf("%#v", *instance)
		assert.Equal(t, instance.AmiID, "ami-0de6d271999e65947")
	}
}

func TestSubnets(t *testing.T) {
	if aws.IsAws() {
		instance, err := aws.InstanceMetadata()
		assert.NoError(t, err)
		subnets := instance.Subnets()
		fmt.Printf("%#v", subnets)
		assert.NotEmpty(t, subnets)
	}
}

func TestDynamic(t *testing.T) {
	if aws.IsAws() {
		dynamic, err := aws.InstanceDynamicData()
		assert.NoError(t, err)
		assert.NotEmpty(t, dynamic)
		accountID := dynamic.InstanceIdentity.Document.AccountID
		fmt.Printf("%#v", accountID)
		assert.NotEmpty(t, accountID)
	}
}
