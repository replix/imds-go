package test

import (
	"fmt"
	//	"encoding/json"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/replix/imds-go/pkg/azure"
	"gitlab.com/replix/imds-go/pkg/cloud"
)

func TestIsAzure(t *testing.T) {
	assert.True(t, azure.IsAzure())
}

func TestIdentify(t *testing.T) {
	assert.Equal(t, cloud.Azure, cloud.Identify())
}

func TestIAzurenstanceMetadata(t *testing.T) {
	metadata, err := azure.InstanceMetadata()
	assert.NoError(t, err)
	fmt.Printf("%#v", *metadata)

	assert.IsType(t, uuid.UUID{}, metadata.Compute.SubscriptionID)
}
