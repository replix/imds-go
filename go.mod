module gitlab.com/replix/imds-go

go 1.15

require (
	github.com/go-resty/resty/v2 v2.4.0
	github.com/google/uuid v1.1.5
	github.com/stretchr/testify v1.7.0
)
