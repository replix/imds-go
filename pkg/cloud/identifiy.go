package cloud

import (
	"gitlab.com/replix/imds-go/pkg/aws"
	"gitlab.com/replix/imds-go/pkg/azure"
)

// Vendor definition
type Vendor string

// Typed constants
const (
	Aws     Vendor = "aws"
	Azure   Vendor = "azure"
	Unknown Vendor = "unknown"
)

// Identify - returns cloud provider name
func Identify() Vendor {

	if aws.IsAws() {
		return Aws
	}

	if azure.IsAzure() {
		return Azure
	}

	return Unknown
}
