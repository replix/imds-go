package azure

import (
	"encoding/json"
	"errors"

	"github.com/go-resty/resty/v2"
)

const azureInstanceMeadataService = "http://169.254.169.254"
const instanceMetadata = "/metadata/instance"

// InstanceMetadata - get instance metadata
func InstanceMetadata() (*Instance, error) {
	client := resty.New().
		SetHostURL(azureInstanceMeadataService)
	response, err := client.
		R().
		SetQueryParams(map[string]string{
			"format":      "json",
			"api-version": "2020-10-01",
		}).
		SetHeader("Metadata", "True").
		SetHeader("Accept", "application/json").
		Get(instanceMetadata)

	var instanceMetadata Instance

	if err != nil {
		return nil, err
	}

	if response.IsSuccess() {
		body := response.Body()
		err = json.Unmarshal(body, &instanceMetadata)
		if err != nil {
			return nil, err
		}
		return &instanceMetadata, nil
	}
	return nil, errors.New("Failed to fetch Azure Instance Metadata")
}

// IsAzure - check if we run on Azure
func IsAzure() bool {
	_, err := InstanceMetadata()
	return err == nil
}
