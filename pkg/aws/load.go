package aws

import (
	"encoding/json"
	"strings"
)

const (
	document = "document"
)

// RecursiveLoad - load recursively all the metadata
func (imds *IMDS) RecursiveLoad() map[string]interface{} {
	return imds.load("")
}

func (imds *IMDS) load(prefix string) map[string]interface{} {
	metadata := map[string]interface{}{}
	text := imds.getInstanceMetadata(prefix)
	keys := strings.Fields(text)
	for _, key := range keys {
		if strings.HasSuffix(key, "/") {
			metadata[strings.TrimSuffix(key, "/")] = imds.load(prefix + key)
		} else {
			metadata[key] = imds.getInstanceMetadata(prefix + key)
		}
	}
	return metadata
}

// LoadInstanceMetadata - load typed instance metadata
func (imds *IMDS) LoadInstanceMetadata() (*Instance, error) {
	metadata := imds.load("")
	data, err := json.Marshal(metadata)
	if err != nil {
		return nil, err
	}

	instance := new(Instance)
	err = json.Unmarshal(data, instance)
	if err != nil {
		return nil, err
	}
	return instance, nil
}

// LoadDynamicData - load typed dynamic data
func (imds *IMDS) LoadDynamicData() (*DynamicData, error) {
	text := imds.getDynamicData(document)
	document := new(Document)
	err := json.Unmarshal([]byte(text), &document)
	if err != nil {
		return nil, err
	}

	instanceIdentity := new(InstanceIdentity)
	instanceIdentity.Document = *document
	dynamic := DynamicData{InstanceIdentity: *instanceIdentity}
	return &dynamic, nil
}
