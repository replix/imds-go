package aws

import (
	"errors"
	"strconv"
	"strings"

	"github.com/go-resty/resty/v2"
)

const (
	instanceMeadataService = "http://169.254.169.254"
	tokenPath              = "/latest/api/token"
	tokenTTLHeader         = "X-aws-ec2-metadata-token-ttl-seconds"
	tokenHeader            = "X-aws-ec2-metadata-token"
	instanceMetadata       = "/latest/meta-data"
	availabilityZone       = "placement/availability-zone"
	availabilityZoneID     = "placement/availability-zone-id"
	region                 = "placement/region"
	networkInterfacesMacs  = "network/interfaces/macs"
	dynamicData            = "/latest/dynamic"
	instanceIdentity       = "instance-identity"
)

// IMDS AWS Instance Metadata Service Client
type IMDS struct {
	client   *resty.Client
	tokenTTL int
	token    *string
}

// New awsIMDS client
func New() IMDS {
	client := resty.New().SetHostURL(instanceMeadataService)
	return IMDS{client, 3600, nil}
}

func (c *IMDS) getToken(tokenTTL int) (*string, error) {
	ttl := strconv.Itoa(c.tokenTTL)
	response, err := c.client.R().SetHeader(tokenTTLHeader, ttl).Put(tokenPath)
	if err != nil {
		return nil, err
	}
	if response.IsSuccess() {
		token := response.String()
		return &token, nil
	}
	return nil, errors.New("Failed to get AWS IMDSv2 token")
}

func (c *IMDS) refreshToken() *IMDS {
	// refresh token
	if c.token == nil {
		c.token, _ = c.getToken(c.tokenTTL)
	}
	return c
}

func (c *IMDS) _get(prefix, path string) (string, error) {
	c.refreshToken()
	request := c.client.R()
	if c.token != nil {
		request.SetHeader(tokenHeader, *c.token)
	}
	path = prefix + "/" + path
	response, err := request.Get(path)
	if err != nil {
		return "", err
	}
	return response.String(), nil
}

func (c *IMDS) getInstanceMetadata(path string) string {
	response, err := c._get(instanceMetadata, path)
	if err != nil {
		response = ""
	}
	return response
}

func (c *IMDS) getDynamicData(path string) string {
	prefix := dynamicData + "/" + instanceIdentity
	response, err := c._get(prefix, path)
	if err != nil {
		response = ""
	}
	return response
}

// AvailabilityZone - get availability zone
func AvailabilityZone() string {
	client := New()
	return client.getInstanceMetadata(availabilityZone)
}

// AvailabilityZoneID - get availability zone id
func AvailabilityZoneID() string {
	client := New()
	return client.getInstanceMetadata(availabilityZoneID)
}

// Region - get region
func Region() string {
	client := New()
	return client.getInstanceMetadata(region)
}

// Subnets - get subnet ids
func Subnets() []string {
	var subnets []string
	var subnet string
	client := New()
	macs := client.getInstanceMetadata(networkInterfacesMacs)

	for _, mac := range strings.Fields(macs) {
		subnet = client.getInstanceMetadata(networkInterfacesMacs + mac + "/subnet-id")
		subnets = append(subnets, subnet)
	}

	return subnets
}

// InstanceMetadata - convenient instance metadata accessor
func InstanceMetadata() (*Instance, error) {
	imds := New()
	return imds.LoadInstanceMetadata()
}

// InstanceDynamicData - convenient instance dynamic data accessor
func InstanceDynamicData() (*DynamicData, error) {
	imds := New()
	return imds.LoadDynamicData()
}

// AccountID - get the account ID of the running instance
func AccountID() (string, error) {
	dynamic, err := InstanceDynamicData()
	if err != nil {
		return "", err
	}

	return dynamic.InstanceIdentity.Document.AccountID, nil
}

// IsAws - check that we are running on AWS
func IsAws() bool {
	imds := New()
	_, err := imds.getToken(1)
	return err == nil
}

// Subnets - get subnets
func (i *Instance) Subnets() []string {
	subnets := []string{}
	for _, mac := range i.Network.Interfaces.Macs {
		subnets = append(subnets, mac.SubnetID)

	}
	return subnets
}

func (i *Instance) VPCs() []string {
	VPCs := []string{}
	for _, mac := range i.Network.Interfaces.Macs {
		VPCs = append(VPCs, mac.VPCID)

	}
	return VPCs
}
